# IN5050 - SISCI non-mandatory assignment

This is a very basic skeleton made by removing all the interesting parts from a
very simple example program.

In the original, the client runs in a loop sending messages to the server and
waiting for the response. The server runs in a loop waiting for new messages.
When a certain message is received, it will transfer some data back before
signaling that it's done. The client will verify that the data is received
correctly.

Look for 'FIXME' in the given code.

Try to fill in the blanks and make something similar, but make any structural
changes you want. The idea is for you to get familiar with SISCI and create a
simple protocol. It's a good idea to start with a simple 'ping-pong' message.

The demo program `scipp` in `/opt/DIS/src/demo/scipp/scipp.c` can be useful to
look at.  It measures the latency from node to node. The client writes a
message to a segment on the server side, and the server waits for a value in
that segment to change in a while loop.

Try to experiment a little. What happens if you reverse the client and server
using `--reverse` to `run.sh`? Or if you run both client and server on the same
node? Try to send some non-trivial amounts of data and use the command protocol
for signaling to the other side when the data is transferred.

## Usage
Set your group id in `common.h` and use `GET_SEGMENTID(id)` to generate
segmentids.  This will ensure that your segmentids don't collide with other
groups. This will be required for the exam as well.

The provided makefile sets the necessary flags for compiling with SISCI and
CUDA.

To run the program, we have provided you with a script `run.sh`. When invoked,
this script copy the source to both nodes, compile and run the programs. The
remote nodeid is automatically passed with the `-r N` parameter. The script
takes a single required parameter, `--tegra` which specifies what tegra to run
on. The script automatically picks the partner PC node. The `--reverse`
parameter can be used to reverse the direction, i.e.  server on the tegra and
client on the PC side.

```
    ./run.sh --tegra <tegra-X> [--reverse]
```

You can modify the script to add parameters if you want.

For the exam, the same or similar script will be given. It will be required
that your solution can be launched by running this script.

## Branches

A draft solution can be found in the `solution` branch.

## Help and feedback

If you run into any problems we encourage you to ask on
[Mattermost](https://mattermost.uio.no/login).
