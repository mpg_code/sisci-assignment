#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sisci_error.h>
#include <sisci_api.h>
#include <assert.h>
#include <errno.h>
#include <getopt.h>

#include "common.h"

void dump_buffer(uint8_t *buf, size_t size) {
    for(unsigned i = 0; i < size; i++) {
        if(i % 8 == 0) {
            printf("\n%4X: ", i);
        }
        printf("%02X ", buf[i]);
    }
}

/* Check all bytes equal to 'should_be' */
int check_data(uint8_t *data, uint8_t should_be, size_t size)
{
    for(unsigned int x = 0; x < size; x++) {
        if( data[x] != should_be) {
            return -1;
        }
    }
    return 0;
}

int main_loop( /* Whatever you need */ )
{
    uint32_t should_be;
    const int num_loops = 100000;

    for (int i = 0; i < num_loops; ++i) {

        /* FIXME Send command to server */

        /* FIXME Wait for response from server */

        /* Verify data correctness */
        should_be = i + 1;

        /* FIXME 
        if(check_data( FIXME , should_be, DATA_SIZE)) {
           printf("Invalid data!\n");
           dump_buffer((uint8_t*) local_seg, sizeof(struct client_segment));

           FIXME Tell server we're quiting
           return;
        }
        */
    }

    /* FIXME Tell server we're quiting */

    return 0;
}

int main(int argc, char *argv[])
{
    unsigned int            localAdapterNo = 0; /* You can just leave this */
    unsigned int            remoteNodeId = 0;   

    int c;

    while ((c = getopt(argc, argv, "r:")) != -1)
    {
        switch (c)
        {
            case 'r':
                remoteNodeId = atoi(optarg);
                break;
        }
    }

    if (remoteNodeId == 0) {
        fprintf(stderr,"Remote node-id is not specified. Use -r <remote node-id>\n");
        exit(EXIT_FAILURE);
    }


    main_loop( /* FIXME */ );

    printf("Exiting\n");

    return 0;
}



