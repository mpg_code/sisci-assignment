#ifndef __COMMON_H
#define __COMMON_H

#include <stdint.h>

#define NO_FLAGS 0
#define NO_CALLBACK NULL

// #define GROUP

#ifndef GROUP
#error Fill in group number in common.h!
#endif

/* GET_SEGMENTIUD(2) gives you segmentid 2 at your groups offset */
#define GET_SEGMENTID(id) ( GROUP << 16 | id )

enum cmd
{
    CMD_INVALID,
    CMD_QUIT,
    CMD_DONE
};

#endif
