#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sisci_error.h>
#include <sisci_api.h>
#include <assert.h>
#include <errno.h>
#include <getopt.h>

#include "common.h"

int main_loop( /* FIXME */ )
{
    int running = 1;
    int cmd;
    while(running)
    {
        /* FIXME Wait for command from client */

        /* FIXME cmd from message */
        cmd = CMD_QUIT;

        switch(cmd) {
            case CMD_QUIT:
                running = 0;
                break;
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    unsigned int            localAdapterNo = 0;   
    unsigned int            remoteNodeId = 0;   

    int c;

    while ((c = getopt(argc, argv, "r:")) != -1)
    {
        switch (c)
        {
            case 'r':
                remoteNodeId = atoi(optarg);
                break;
        }
    }

    if (remoteNodeId == 0) {
        fprintf(stderr,"Remote node-id is not specified. Use -r <remote node-id>\n");
        exit(EXIT_FAILURE);
    }

    main_loop( /* FIXME */ );

    printf("Exiting\n");

    return 0;
}



